# 👋🏼 Hey there!
Hey, nice to meet you! Thanks for stopping by!

My name is [Bryan Ross](https://bryanross.me/) and I’ve been building platforms (as a product) for over 20 years. I’ve held numerous roles in engineering, consultancy and senior leadership across a variety of industries. I co-founded my own SaaS company back in 2001, formed and led the Technology Products division at [Sky TV](https://www.sky.com/), and currently work as a [Field CTO](https://handbook.gitlab.com/job-families/sales/solutions-architect/#field-cto) at [GitLab](https://about.gitlab.com/company/team/#bryanrossuk) where, as one of only a handful of distinguished technology executives globally, I’m privileged to work with some of GitLab’s largest and most interesting customers.

_**I operate in that fuzzy space where technology meets business and people.**_

Outside of work, I find balance in the non-digital world, immersing myself in the beauty of Scotland's shores through sailing and scuba diving. I live with my wife and three rambunctious boys on a smallholding on the outskirts of Edinburgh, Scotland.

## 📚 Things I like to think about...
I write and talk about the things that I’ve been asked about by customers, colleagues and friends. Topics can be varied, but will generally include:

* ⌨️ **Platform Engineering**. Demystifying yet another overloaded technology term through simple explanations, well-researched advice, and real-world examples.

* 🛍️ **Platform-as-a-Product**. I’ll show you how you can grow adoption of technology by building an authentic brand, tied to a successful “go to market” strategy, through a customer-centric mindset that will help you build an army of happy customers willing to advocate for your platform.

* 🚀 **Digital Transformation**. Practical advice to influencing change in your organisation. What I’ve seen work and fail in organisations across a broad spectrum of industries.

* 💼 **Leadership**. I’ll share some of the sage advice I’ve been received that has helped grow my career over the past 30 years. Suitable for new leaders and seasoned veteran alike!

**_If you’d like help unlocking business value from your IT stack, stick around!_**


## 🤗 Collaboration
I love working with other people!  Would you like me to speak at your event, participate in a podcast, write an article, or perhaps review something you've been working on?  Great!  Get in touch through one the social media platforms below.

## 📨 Let’s Be Friends!
I post regularly to the following platforms.  On social media, I re-post things that I've found interesting and on my own website, I produce longer form articles on a monthly basis.

* My Newsletter: https://newsletter.bryanross.me/
* My Website: https://bryanross.me/ 
* LinkedIn: https://www.linkedin.com/in/bryanross/
* Twitter/X: https://twitter.com/bryanrossuk
* Mastodon: https://hachyderm.io/@bryanrossUK/ 
* GitLab: https://about.gitlab.com/company/team/#bryanrossuk 

## 🎤 Public Speaking
You can review a list of my publications and public speaking engagements [here](https://docs.google.com/document/d/1TUVWQx4kC81mJ1G9k5OWUUqRoTN2h_JwGF9VZeH_SXE/edit?usp=drive_link) 
